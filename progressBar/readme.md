Simple Play 2.2 example for long running process with progress bar
=====================================

This is scala version of Ciaran Fisher's answer to the [question](http://stackoverflow.com/questions/11398495/long-running-process-with-progress-bar-example-playframework-2)
-------------------------------------
The system works by first creating a unique Actor to process a "report" (when the generate page is loaded).
This actor spawns a child actor which reports its progress back to the parent.
The parent actor is then polled via JavaScript for the status of the child thread.

Once the child has finished it is terminated and once the Parent detects the child is finished it terminates itself.
