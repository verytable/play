package controllers

import play.api.mvc._
import akka.pattern.ask
import play.libs.Akka
import actors.{MyGeneratorMaster, ConfigMessage, ResultMessage, StatusMessage}
import akka.actor.Props

object Application extends Controller {

  def index = Action {
    Ok(views.html.index("Your new application is ready.")("Title")("id"))
  }

  def generateReport = Action  {
    val myActor = Akka.system().actorOf(Props(new MyGeneratorMaster))
    println(myActor.path)
    myActor ! ConfigMessage("blarg message")
    Ok(views.html.index("blarg")("title")(myActor.path.name))
  }

  def status(uuid: String) = Action {

    val newUuid = "akka://application/user/" + uuid
    val myActor = Akka.system().actorFor(newUuid)

    if (myActor.isTerminated) {
      Ok("Report generated - All actors termindated")
    } else {
      val response = ask(myActor, StatusMessage)(3000).mapTo[ResultMessage]
      Ok(response.toString)
    }
  }
}