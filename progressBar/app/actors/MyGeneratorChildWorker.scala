package actors

import akka.actor.Actor

/**
 * Created by arseny on 4/24/14.
 */
class MyGeneratorChildWorker extends Actor {

  override def postStop() = {
    super.postStop()
    println("Child killed")
  }

  def receive = {
    case ConfigMessage(message) =>
      println("Created child worker")
      println("Doing work:")
      try {
        for (i <- 0 to 100) {
          sender ! StatusUpdate(i.toLong)
          var j = 1
          while (j < 1E8) {
            j = j + 1
          }
        }
      } catch {
        case e: Exception =>
          println(e)
      }
      println("Done work")
  }
}
