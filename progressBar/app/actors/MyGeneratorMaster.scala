package actors

import akka.actor.{Props, ActorRef, Actor}

/**
 * Created by arseny on 4/24/14.
 */

sealed trait Message
case class ConfigMessage(message: String) extends Message
case class StatusUpdate(i: Long) extends Message
case class StatusMessage() extends Message
case class ResultMessage(message: String) extends Message

class MyGeneratorMaster extends Actor {

  private var completed = 0L

  override def postStop() {
    super.postStop()
    println("Master killed")
  }

  def receive = {
    case ConfigMessage(message) =>
      println("Received Config")
      val child = context.actorOf(Props(new MyGeneratorChildWorker))
      child ! ConfigMessage("Do something!")
      child ! akka.actor.PoisonPill.getInstance
    case StatusUpdate(i) =>
      println(s"Got status update $i")
      completed = i
    case StatusMessage =>
      println("Got status message")
      sender ! ResultMessage("Status: " + completed + "%")
      if (completed == 100L) {
        self ! akka.actor.PoisonPill.getInstance
      }
  }
}
