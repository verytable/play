application.name = Каталог продуктов
name = Название
description = Описание

products.details = Продукт: {0}

products.form = Информация о продукте
products.new = (новый)
products.new.command = Новый
products.new.submit = Добавить
products.new.success = Успешно добавлен {0}

validation.errors = Пожалуйста исправьте ошибки в форме
validation.ean.duplicate = Продукт с таким кодом уже существует